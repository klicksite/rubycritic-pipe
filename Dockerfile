FROM ruby:2.7-alpine

RUN apk update && apk del gmp-dev && apk add gmp-dev && gem install rubycritic

COPY pipe.sh /

RUN chmod +x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
