#!/bin/sh

export WORK_DIR=$PWD

mkdir -p tmp

if [ -z $RUBYCRITIC_MINIMUM_SCORE ]; then
  export RUBYCRITIC_MINIMUM_SCORE=100
fi

if [ "$CI" = "CI" ]; then
  rubycritic \
    --no-browser \
    --mode-ci \
    --minimum-score $RUBYCRITIC_MINIMUM_SCORE | tee /tmp/rubycritic.log
else
  rubycritic \
    --path "$PWD/tmp/rubycritic-pipe" \
    --minimum-score $RUBYCRITIC_MINIMUM_SCORE | tee /tmp/rubycritic.log
fi

RUBYCRITIC_RESULT=`cat /tmp/rubycritic.log | grep Score`
RUBYCRITIC_SCORE=`echo $RUBYCRITIC_RESULT | cut -d " " -f 2 | tr -d '()'`
RUBYCRITIC_SCORE=${RUBYCRITIC_SCORE%.*}

echo ""
echo "RESULT"
echo "======"
echo "$RUBYCRITIC_RESULT"

if [[ $RUBYCRITIC_SCORE -lt $RUBYCRITIC_MINIMUM_SCORE ]]; then
  echo ""
  echo "Fail!"
  exit 1
fi

echo "Minimum score: $RUBYCRITIC_MINIMUM_SCORE"
echo ""
echo "Success!"
