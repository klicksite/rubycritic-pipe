# Rubycritic Pipe

Static analysis your code with rubycritic

```yml
- pipe: docker://klickpages/rubycritic-pipe:latest
  variables:
    RUBYCRITIC_MINIMUM_SCORE: 100 // default is 100. you can also use a environment variable: $MY_MINIMUM_SCORE
```
